<?php

require_once ('../process/dbh.php');

$sql = "SELECT employee_checkinout.id,employee_checkinout.token,employee_checkinout.start,employee_checkinout.end,employee.firstName from `employee_checkinout` join employee where employee_checkinout.id = employee.id";
//echo "$sql";
$result = mysqli_query($conn, $sql);

?>



<html>
<head>
	<title>Employee Leave | Admin Panel | DonaldiDemiraj</title>
	<link rel="stylesheet" type="text/css" href="css/styleview.css">
</head>
<body>
	
	<header>
		<nav>
			<h1>EMS</h1>
			<ul id="navli">
				<li><a class="homeblack" href="adminhome.php">HOME</a></li>
				
				<li><a class="homeblack" href="addemp.php">Add Employee</a></li>
				<li><a class="homeblack" href="viewemp.php">View Employee</a></li>
				<li><a class="homered" href="admin/employeecheckinout.php">Employee CheckIn/CheckOut</a></li>
				<li><a class="homeblack" href="../alogin.html">Log Out</a></li>
			</ul>
		</nav>
	</header>
	 
	<div class="divider"></div>
	<div id="divimg">
		<table>
			<tr>
				<th>Emp. ID</th>
				<th>Token</th>
				<th>Name</th>
				<th>Start Time</th>
				<th>End Time</th>
			</tr>

			<?php
				while ($employee = mysqli_fetch_assoc($result)) {

				$date1 = new DateTime($employee['start']);
				$date2 = new DateTime($employee['end']);
				$interval = $date1->diff($date2);
				$interval = $date1->diff($date2);
				//echo "difference " . $interval->days . " days ";

					echo "<tr>";
					echo "<td>".$employee['id']."</td>";
					echo "<td>".$employee['token']."</td>";
					echo "<td>".$employee['firstName'].	"</td>";
					
					echo "<td>".$employee['start']."</td>";
					echo "<td>".$employee['end']."</td>";

				}


			?>

		</table>
		
	</div>
</body>
</html>